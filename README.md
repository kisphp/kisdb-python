# Kisphp Database connection layer (test purposes only)

This library supports only MySQL and is intended only for tests and quick development demos. 

> Do not use it in production since there is no security sanitization implemented !

## Installation

```bash
pip install git+https://gitlab.com/kisphp/kisdb-python.git
```

## Usage

```python
#!/usr/bin/env python

from kisdb import Kisdb

db = Kisdb(
    host='localhost',
    user='root',
    password='',
    database='test',
    port=3306
)

insert_id = db.insert('users', **{
    'first_name': 'John',
    'last_name': 'Doe',
    'email': 'john@doe.com',
    'password': 'demo2'
})
print(insert_id)

affected_rows = db.update('users', condition={'id': 2}, **{
    'last_name': 'DoeChanged'
})
print(affected_rows)

results = db.get_row("SELECT * FROM users")
print(results)
```