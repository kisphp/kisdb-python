#!/usr/bin/env python

from mysql.connector import connect


class Kisdb:
    def __init__(self, host, user, password, database, port=3306):
        self.mydb = connect(
            host=host,
            user=user,
            password=password,
            database=database,
            port=port
        )
        self.cursor = self.mydb.cursor(dictionary=True)

    def _execute(self, raw_sql):
        self.cursor.execute(raw_sql)

    def get_row(self, raw_sql):
        self._execute(raw_sql)
        return self.cursor.fetchall()

    def insert(self, table_name, **kwargs):
        sql = f"INSERT INTO {table_name } "
        sql += "SET "
        params = []
        for x in kwargs:
            params.append("{} = '{}'".format(x, kwargs[x]))

        sql += ', '.join(params)

        self._execute(sql)
        self.mydb.commit()

        return self.cursor.lastrowid

    def update(self, table_name, condition: dict, **kwargs):
        sql = f"UPDATE {table_name} "
        sql += "SET "
        params = []
        for x in kwargs:
            params.append("{} = '{}'".format(x, kwargs[x]))

        sql += ', '.join(params)

        sql += ' WHERE '

        cond_params = []
        for i in condition:
            cond_params.append('{0} = \'{1}\''.format(i, condition[i]))

        sql += ' AND '.join(cond_params)

        self._execute(sql)
        self.mydb.commit()

        return self.cursor._affected_rows

    def query(self, raw_sql):
        pass