#!/usr/bin/env python

from setuptools import find_packages, setup

setup(
    name='kisdb',
    description='Kisphp python mysql library',
    version='0.1.0',
    author='Bogdan Rizac',
    author_email='mariusbogdan83@gmail.com',
    scripts=['kisdb/kisdb.py'],
    python_requires='>=3.6',
    packages=find_packages()
)
